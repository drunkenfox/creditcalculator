package ru.drunkenfox.creditcalc;

/**
 * Created by Ilshat on 01.08.2016.
 */
public class Calculator {
    /**
     * Total requested credit
     */
    private float _total;

    /**
     * Procentage
     */
    private float _procent;

    /**
     * Quantity of Month
     */
    private int _month;

    public Calculator(float total, float procent, int month){
        this._total = total;
        this._procent = procent;
        this._month = month;

    }

    /**
     * Property. Get quantity of month
     * @return
     */
    public int get_month() {
        return _month;
    }

    /**
     * Property. Set quantity of month
     * @param _month
     */
    public void set_month(int _month) {
        this._month = _month;
    }

    /**
     * Property. Get requested total
     * @return
     */
    public float get_total() {
        return _total;
    }

    /**
     * Property. Set requested total
     * @param _total
     */
    public void set_total(float _total) {
        this._total = _total;
    }

    /**
     * Property. Get yearly percentage
     * @return
     */
    public float get_procent() {
        return _procent;
    }

    /**
     * Property. Set yearly percentage
     * @param _procent
     */
    public void set_procent(float _procent) {
        this._procent = _procent;
    }

    /**
     * Method. Calculate monthly payments rate
     * @return monthly payments rate
     */
    public float MonthlyPayments(){
        float mpay;
        double mp;
        double month = (double)_month;
        //Monthly percentage
        mp = (_procent/100)/12;

        mpay = (float)(_total*(mp + mp/(Math.pow((1.0+mp),month)-1)));
        return mpay;
    }

    /**
     * Method. Calculates overpayments rate
     * @return overpayments rate
     */
    public float OverPay(float monthlyPayment){
        return monthlyPayment*_month - _total;
    }


}
