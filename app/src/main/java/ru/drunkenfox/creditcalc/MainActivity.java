package ru.drunkenfox.creditcalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText creditEditText;
    EditText percentageEditView;
    EditText monthQntEditText;
    TextView monthlyTextView;
    TextView overPayTextView;
    Calculator calc;
    float credit;
    float prc;
    float monthlyPay;
    float overPay;
    int monthQnt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        creditEditText = (EditText)findViewById(R.id.creditEditView);
        percentageEditView = (EditText)findViewById(R.id.procentEditView);
        monthQntEditText = (EditText)findViewById(R.id.monthQntEditText);
        monthlyTextView = (TextView)findViewById(R.id.monthlyTextView);
        overPayTextView = (TextView)findViewById(R.id.overPayTextView);


        calc = new Calculator(credit, prc, monthQnt);

    }

    public void onSubmitClick(View v){
        onKeyPress();
    }

    //Get data from form
    private void GetData(){
        String str_credit = creditEditText.getText().toString();
        String str_prc = percentageEditView.getText().toString();
        String str_monthQnt = monthQntEditText.getText().toString();

        //Check not empty
        if (str_credit.length() > 0){
            credit = Float.valueOf(str_credit);
        }

        if (str_prc.length() > 0){
            prc = Float.valueOf(str_prc);
        }

        if (str_monthQnt.length() > 0){
            monthQnt = Integer.valueOf(str_monthQnt);
        }

        if(str_credit.length() == 0 || str_prc.length() == 0 || str_monthQnt.length() == 0){
            String message = str_credit.length() == 0?"Введите сумму кредита":"";
                   message +=  str_prc.length() == 0?" Введите процентную ставку":"";
                   message +=  str_monthQnt.length() == 0?" Введите срок кредитования":"";
            Toast.makeText(this, message,Toast.LENGTH_LONG).show();
        }

    }

    //Get data from view, calculate, set data on view
    private void onKeyPress(){

        GetData();
        calc.set_total(credit);
        calc.set_procent(prc);
        calc.set_month(monthQnt);

        monthlyPay = calc.MonthlyPayments();
        overPay = calc.OverPay(monthlyPay);
        monthlyTextView.setText("Ежемесячный платеж: " + String.format("%.02f", monthlyPay));
        overPayTextView.setText("Переплата: " + String.format("%.02f", overPay));
    }
}
